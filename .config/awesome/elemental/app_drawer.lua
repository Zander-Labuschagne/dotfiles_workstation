local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local apps = require("apps")
local icons = require("icons")
-- local naughty = require("naughty")

local helpers = require("helpers")

-- Appearance
-- local button_size = beautiful.app_drawer_icon_size or dpi(100)
--
local keybinds = {}

-- Helper function that generates the clickable buttons
local create_button2 = function(symbol, symbol_hover, command)
    local icon = wibox.widget {
        -- markup = helpers.colorize_text(symbol, color),
        image = symbol,
        align = "center",
        valign = "center",
        -- font = "icomoon 50",
        forced_width = dpi(180),
        forced_height = dpi(200),
        widget = wibox.widget.imagebox
    }

    local button = wibox.widget {
        {
            nil,
            icon,
            expand = "none",
            layout = wibox.layout.align.horizontal
        },
        forced_height = button_size,
        forced_width = button_size,
        -- border_width = dpi(8),
        -- border_color = button_bg,
        shape = helpers.rrect(dpi(20)),
        -- bg = button_bg .. "FF",
        widget = wibox.container.background
    }

    -- Bind left click to run the command
    button:buttons(gears.table.join(
        awful.button({ }, 1, function ()
            command()
        end)
    ))

    -- Highlight on hover
    button:connect_signal("mouse::enter", function ()
        icon.image = symbol_hover
    end)
    button:connect_signal("mouse::leave", function ()
        icon.image = symbol
    end)

    -- Use helper function to change the cursor on hover
    helpers.add_hover_cursor(button, "hand1")

    return button
end
-- Helper function that creates buttons given a text symbol, color, hover_color
-- and the command to run on click.
local function create_button(symbol, color, hover_color, cmd, key)
    local icon = wibox.widget {
        markup = helpers.colorize_text(symbol, color),
        align = "center",
        valign = "center",
        font = "icomoon 50",
        forced_width = dpi(180),
        forced_height = dpi(200),
        widget = wibox.widget.textbox
    }

    -- Press "animation"
    icon:connect_signal("button::press", function(_, _, __, button)
        if button == 3 then
            icon.markup = helpers.colorize_text(symbol, hover_color.."55")
        end
    end)
    icon:connect_signal("button::release", function ()
        icon.markup = helpers.colorize_text(symbol, hover_color)
    end)

    -- Hover "animation"
    icon:connect_signal("mouse::enter", function ()
        icon.markup = helpers.colorize_text(symbol, hover_color)
    end)
    icon:connect_signal("mouse::leave", function ()
        icon.markup = helpers.colorize_text(symbol, color)
    end)

    -- Change cursor on hover
    helpers.add_hover_cursor(icon, "hand1")

    -- Adds mousebinds if cmd is provided
    if cmd then
        icon:buttons(gears.table.join(
            awful.button({ }, 1, function ()
                cmd()
            end),
            awful.button({ }, 3, function ()
                cmd()
            end)
        ))
    end

    -- Add keybind to dict, if given
    if key then
        keybinds[key] = cmd
    end

    return icon
end

-- Create app buttons
local browser = create_button("", x.color3, x.color11, apps.browser, "w")
local discord = create_button("", x.color5, x.color13, apps.discord, "d")
local telegram = create_button("", x.color4, x.color12, apps.telegram, "t")
local mail = create_button("", x.color6, x.color14, apps.mail, "m")
local files = create_button("", x.color3, x.color11, apps.file_manager, "f")
local gimp = create_button("", x.color5, x.color13, apps.gimp, "g")
local youtube = create_button("", x.color1, x.color9, apps.youtube, "y")
local networks = create_button("", x.color3, x.color11, apps.networks, "n")
local passwords = create_button("", x.color1, x.color9, apps.passwords, "p")
local night_mode = create_button("", x.color1, x.color9, apps.night_mode, "x")
local record = create_button("", x.color4, x.color12, apps.record, "r")
local lutris = create_button("", x.color6, x.color14, apps.lutris, "l")
-- local steam = create_button("", x.color2, x.color10, apps.steam, "s")
local org = create_button("", x.color2, x.color10, apps.org, "o")
-- local compositor = create_button("", x.color5, x.color13, apps.compositor, "z")

local firefox = create_button2(icons.image.firefox, icons.image.firefox_select, apps.browser)
local vivaldi = create_button2(icons.image.vivaldi, icons.image.vivaldi_select, apps.vivaldi)
-- local epiphany = create_button(icons.image.epiphany, epiphany_command)
local youtube = create_button2(icons.image.youtube, icons.image.youtube_select, apps.youtube)
local reddit = create_button2(icons.image.reddit, icons.image.reddit_select, apps.reddit)
-- local vpn = create_button2(icons.image.protonvpn, icons.image.protonvpn_select, apps.vpn)
local files = create_button2(icons.image.files, icons.image.files_select, apps.file_manager)
local cloud = create_button2(icons.image.tresorit_new, icons.image.tresorit_new_select, apps.tresorit)
local gdrive = create_button2(icons.image.gdrive, icons.image.gdrive_select, apps.gdrive)
local torrents = create_button2(icons.image.bittorrent, icons.image.bittorrent_select, apps.deluge)
local mail = create_button2(icons.image.protonmail, icons.image.protonmail_select, apps.mail)
local steam = create_button2(icons.image.steam, icons.image.steam_select, apps.steam)
-- -- local lutris = create_button(icons.image.games, lutris_command)
local discord = create_button2(icons.image.discord, icons.image.discord_select, apps.discord)
-- -- local spotify = create_button2(icons.image.spotify, icons.image.spotify_select, spotify_command)
-- -- local nvim = create_button(icons.image.nvim, nvim_command)
local sublime_text = create_button2(icons.image.sublime_text, icons.image.sublime_text_select, apps.sublime_text)
-- -- local codium = create_button(icons.image.codium, codium_command)
local qtcreator = create_button2(icons.image.qtcreator, icons.image.qtcreator_select, apps.qt)
-- local sailfish_studio = create_button(icons.image.sailfish2, apps.sailfish)
local clion = create_button2(icons.image.clion, icons.image.clion_select, apps.clion)
local intellij = create_button2(icons.image.intellij, icons.image.intellij_select, apps.intellij)
local matlab = create_button2(icons.image.matlab, icons.image.matlab_select, apps.matlab)
-- -- local unreal_engine = create_button(icons.image.ue, ue_command)
-- -- local godot = create_button(icons.image.godot, apps.godot)
local musescore = create_button2(icons.image.musescore, icons.image.musescore_select, apps.musescore)
local sublime_merge = create_button2(icons.image.sublime_merge, icons.image.sublime_merge_select, apps.sublime_merge)
local meld = create_button2(icons.image.meld, icons.image.meld, apps.meld)
local color = create_button2(icons.image.colorpicker, icons.image.colorpicker_select, apps.colorpicker)
local settings = create_button2(icons.image.settings, icons.image.settings_select, apps.settings)
local compositor = create_button2(icons.image.compositor, icons.image.compositor_select, apps.compositor)

local restart_awesome = create_button("", x.color4, x.color12)
restart_awesome:buttons(gears.table.join(
    awful.button({ }, 1, awesome.restart)
))

-- Create the widget
app_drawer = wibox({visible = false, ontop = true, type = "dock"})
awful.placement.maximize(app_drawer)

app_drawer.bg = "#00000000"
-- app_drawer.bg = beautiful.app_drawer_bg or x.background or "#111111"
app_drawer.fg = beautiful.app_drawer_fg or x.foreground or "#FEFEFE"

-- Add app drawer or mask to each screen
for s in screen do
    if s == screen.primary then
        s.app_drawer = app_drawer
    else
        s.app_drawer = helpers.screen_mask(s, beautiful.lock_screen_bg or beautiful.exit_screen_bg or x.background)
    end
end

local function set_visibility(v)
    for s in screen do
        s.app_drawer.visible = v
    end
end

local app_drawer_grabber
function app_drawer_hide()
    awful.keygrabber.stop(app_drawer_grabber)
    set_visibility(false)
end

function app_drawer_show()
    -- naughty.notify({text = "starting the keygrabber"})
    app_drawer_grabber = awful.keygrabber.run(function(_, key, event)
        local invalid_key = false

        -- Debug
        -- naughty.notify({ title = event, text = key })
        -- if event == "press" and key == "Alt_L" or key == "Alt_R" then
        --     naughty.notify({ title = "you pressed alt" })
        -- end
        -- if event == "release" and key == "Alt_L" or key == "Alt_R" then
        --     naughty.notify({ title = "you released alt" })
        -- end

        if event == "release" then return end

        if keybinds[key] then
            keybinds[key]()
        else
            invalid_key = true
        end

        if not invalid_key or key == 'Escape' then
            app_drawer_hide()
        end
    end)

    set_visibility(true)
end

app_drawer:buttons(gears.table.join(
    -- Left click - Hide app_drawer
    awful.button({ }, 1, function ()
        app_drawer_hide()
    end),
    -- Right click - Hide app_drawer
    awful.button({ }, 2, function ()
        app_drawer_hide()
    end),
    -- Middle click - Hide app_drawer
    awful.button({ }, 2, function ()
        app_drawer_hide()
    end)
))

local function create_stripe(widgets, bg)
    local buttons = wibox.widget {
        -- spacing = dpi(20),
        layout = wibox.layout.fixed.horizontal
    }

    for _, widget in ipairs(widgets) do
        buttons:add(widget)
    end

    local stripe = wibox.widget {
        {
            nil,
            {
                nil,
                buttons,
                expand = "none",
                layout = wibox.layout.align.horizontal
            },
            expand = "none",
            layout = wibox.layout.align.vertical
        },
        bg = bg,
        widget = wibox.container.background
    }

    return stripe
end

app_drawer:setup {
    -- Background
    {
        -- Stripes
        create_stripe({firefox, vivaldi, mail, youtube, reddit}, "#00000000"),
        create_stripe({files, cloud, gdrive, torrents}, beautiful.bg_focus.."20"),
        create_stripe({sublime_text, qtcreator, clion, intellij, matlab}, "#00000000"),
        create_stripe({sublime_merge, meld, color, steam, discord}, beautiful.bg_focus.."20"),
        create_stripe({record, musescore, compositor, settings, restart_awesome}, "#00000000"),
        layout = wibox.layout.flex.vertical
    },
    bg = x.background,
    -- bg = x.background.."AA",
    -- bg = "#00000000",
    widget = wibox.container.background
}

